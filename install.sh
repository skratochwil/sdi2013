#!/bin/bash
cd

export sdi_userdir="${HOME}/"

userdir="$(dirname "$scriptFolder")"
basedir=$userdir"/base/"

echo "======================================================"
echo "Copy installscripts ..."
echo "======================================================"
cp /vagrant/config ${sdi_userdir}

echo "======================================================"
echo "Calling . config..."
echo "======================================================"
. ${sdi_userdir}/config

echo "======================================================"
echo "Creating folders..."
echo "======================================================"
mkdir -p "$sdi_basedir"
mkdir -p "$sdi_data"
mkdir -p "$sdi_tmp"
mkdir -p "$sdi_cd_dir"
mkdir -p "$sdi_tools"
mkdir -p "${sdi_cd_dir}/sdios"
mkdir -p "${sdi_cd_dir}/boot/grub"

echo "======================================================"
echo "Copy config file..."
echo "======================================================"
cp config "$sdi_tools/"

echo "======================================================"
echo "Selecting gdb port number..."
echo "======================================================"
expr $RANDOM % 64511 + 1024 > "${sdi_tools}/.gdbport"

echo "======================================================"
echo "Obtain IDL4 sources, compile and install..."
echo "======================================================"
cd "$sdi_tmp"
wget -nv -nv "$sdiLink_IDL4" -O idl4_source.zip

#unzip source
cd "$sdi_data"
unzip "${sdi_tmp}/idl4_source.zip"

cd "${sdi_data}/idl4-master"
./autogen.sh
./configure
sudo make install

echo "======================================================"
echo "Obtain kernel sources and compile kernel..."
echo "======================================================"
# download source
cd "$sdi_tmp"
wget "$sdiLink_L4_Kernel" -O l4ka_source.zip

# unzip source
cd "$sdi_data"
unzip "${sdi_tmp}/l4ka_source.zip"

# create links for more uniform fs layout
export tmp_l4_exact_name="`/bin/ls | grep l4ka`"
ln -s "${sdi_data}/$tmp_l4_exact_name" "$sdi_l4"

## Kernel
# setup build directory
cd "${sdi_l4}/"
mkdir build

cd kernel
make BUILDDIR="${sdi_l4}/build/kernel"

cd "${sdi_l4}/build/kernel"

# download l4 config
wget "$sdiLink_L4_Config" -O "config/config.out"

# Change gcc version to gcc3
#echo "CC=/tools/bin/gcc3" >> Makefile

# compile kernel
make clean
make batchconfig
make

# copy kernel to cd_dir
cp x86-kernel "${sdi_cd_dir}/sdios/"

## User
cd "${sdi_l4}/user/"
autoheader; autoconf

mkdir "${sdi_l4}/build/user"
cd "${sdi_l4}/build/user"

"${sdi_l4}/user/configure" --prefix="$sdi_l4_user" --libexecdir="$sdi_l4_user"
make install

# copy kickstart to cd_dir
cp util/kickstart/kickstart "${sdi_cd_dir}/sdios/"
cp serv/sigma0/sigma0 "${sdi_cd_dir}/sdios/"

echo "======================================================"
echo "Setting up default menu.lst ..."
echo "======================================================"
cp "/vagrant/stage2_eltorito" "${sdi_cd_dir}/boot/grub"

# create menu.lst
menu_lst_content="serial --unit=0 --speed=57600\n
terminal serial console\n\r
\n
default 0\n
\n
title=SDIOS \n
\t	kernel=(cd)/sdios/kickstart\n
\t	module=(cd)/sdios/x86-kernel\n
\t	module=(cd)/sdios/sigma0\n
\t	module=(cd)/sdios/root\n
\t	module=(cd)/sdios/test\n"

echo -ne $menu_lst_content | sed 's/^ *//g' > ${sdi_cd_dir}/boot/grub/menu.lst

echo "======================================================"
echo "Installing and compiling SDIOS..."
echo "======================================================"
# create links for more uniform fs layout
ln -s "/vagrant/sdios" "$sdi_sdios"

## Compile SDI OS:

cd "$sdi_sdios"
autoheader; autoconf
mkdir build
cd build 
../configure --prefix="${sdi_cd_dir}/sdios/" --with-l4dir="$sdi_l4_user"
# compile SDI OS
make && make install


echo "======================================================"
echo "Installing tools..."
echo "======================================================"
cp -r ${sdi_sdios}/misc/tools/* ${sdi_tools}/
